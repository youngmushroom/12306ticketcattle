type TemplateType = "basic" | "image" | "list" | "progress";
interface NotificationOptions {
    type: TemplateType;
    message: string;
    title: string;
    iconUrl?: string;
    contextMessage?: string;
    priority?: number;
    eventTime?: number;
    buttons?: {
        title: string;
        iconUrl: string;
    },
    imageUrl: string;
    items: { title: string, messsage: string } | Array<{ title: string, messsage: string }>;
    progress: number;
}
interface Notifications {
    create(id?: string, options?: NotificationOptions): Promise<string>;
    update(id?: string, options?: NotificationOptions): Promise<boolean>;
    getAll();
    clar();
}

interface Cookie {
    domain: string;
    expirationDate: number;
    firstPartyDomain: string;
    hostOnly: boolean;
    name: string;
    path: string;
    secure: string;
    session: boolean;
    sameSite: any;
    storeId: string;
    value: string;
}
interface Cookies {
    get(details: {
        name: string,
        url: string,
        firstPartyDomain?: string,
        storeId?: string,
    }): Promise<{}>;
    getAll();
    set();
    remove();
    getAllCookieStores();
}
interface I18n {
    getAcceptLanguages(): Promise<string[]>;
    getMessage(messageName: string, substitutions?: string | string[]): string;
    getUILanguage(): string;
    detectLanguage(text: string): Promise<{
        isReliable: boolean,
        languages: Array<{
            language: string,
            percentage: number
        }>
    }>;
}
interface Runtime {
    getURL(path: string);
}
interface Browser {
    i18n: I18n;
    notifications: Notification;
    runtime: Runtime;
}
declare var browser: Browser;

const path = require('path');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');
const ZipPlugin = require('zip-webpack-plugin');
const webpack = require('webpack');
var LessAutoprefix = require('less-plugin-autoprefix');
module.exports = {
	entry: {
		app: "./src/index.ts",
		"plugin/12306": "./src/plugins/12306/index.ts",
		"plugin/xjHospital": "./src/plugins/xj/index.ts",
		background: "./src/background.page.ts",
	},
	devtool: 'source-map',
	module: {
		rules: [{
			test: /\.css$/,
			use: [
				'style-loader',
				'css-loader'
			]
		},
		{
			test: /\.(less)$/,
			use: [{
				loader: MiniCssExtractPlugin.loader
			},
			{
				// translates CSS into CommonJS
				loader: 'css-loader'
			},
			{
				// compiles Less to CSS
				loader: 'less-loader',
				options: {
					plugins: [
						new LessAutoprefix({
							browsers: ['last 2 versions']
						})
					]
				},
			},
			]
		}, {
			test: /\.schema\.json$/,
			use: 'json-loader',
			exclude: /node_modules/
		}, {
			test: /\.tsx?$/,
			use: 'ts-loader',
			exclude: /node_modules/
		}, {
			test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
			loader: 'url-loader',
		}, {
			test: /\.(temp\.html|svg)/,
			loader: 'raw-loader',
		}]
	},
	optimization: {
		splitChunks: {
			chunks: 'all',
			minChunks: 1,
			name: true,
			cacheGroups: {
				vendors: {
					filename: '[name].bundle.min.js',
					name: "vendor",
					test: /[\\/]node_modules[\\/]/,
					reuseExistingChunk: true,
					chunks: 'all'
				},
			}
		},
		runtimeChunk: 'single',
	},
	resolve: {
		extensions: [
			'.ts',
			'.js'
		],
		alias: {
			// resolve vue compiler version
			'vue$': 'vue/dist/vue.esm.js'
		}
	},
	output: {
		filename: '[name].bundle.min.js',
		path: path.resolve(__dirname, 'dist')
	},
	plugins: [
		new webpack.HashedModuleIdsPlugin(),
		new CleanWebpackPlugin(["dist"]),
		new CopyWebpackPlugin([{
			from: "manifest.json",
		}, {
			from: "less/trian.png",
			to: "img/trian.png",
		}, {
			from: "schema/*.json",
		}, {
			from: "_locales",
			to: "_locales",
		}]),
		new MiniCssExtractPlugin({
			filename: "css/[name].bundle.min.css",
		}),
		new HtmlWebpackPlugin({
			template: 'html/main.content.html',
			chunks: ["runtime", "vendor", "app", "plugin/12306", "plugin/xjHospital"],
			filename: "html/main.content.html",
		}),
		new HtmlWebpackPlugin({
			template: 'html/browser.action.html',
			filename: "html/browser.action.html"
		}),
		new HtmlWebpackPlugin({
			template: 'html/background.page.html',
			chunks: ["background"],
			filename: "html/background.page.html"
		}),
		// new ZipPlugin({
		// 	filename: 'web-ext.zip',
		// })
	],
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		index: "html/main.content.html",
	},
	watch: true,
};
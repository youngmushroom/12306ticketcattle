import Vue from "vue";
import "./common/bootstrap";
import "./common/component/SVGIcon";
import "./common/component/TimeLine";
import "./styles/app.less";
import { IPlugin } from "./types";

import Component from "vue-class-component";
import { State } from "vuex-class";
import template from "./app.temp.html";
import { StateType } from "./state";

@Component({
    template,
})
class Root extends Vue {
    @State(StateType.Active)
    active: boolean;
    @State(StateType.Timestamp)
    stamp: number;
    @State(StateType.ActivePlugin)
    pluginName: string;
    @State(StateType.InstalledPlugins)
    plugins: { [key: string]: IPlugin };
    onOpen() {
        this.$store.commit(StateType.Active, true);
    }

    onClose(e: Event) {
        this.$store.commit(StateType.Active, false);
        e.preventDefault();
    }
    get component() {
        return this.$store.state.ActivePlugin;
    }
}
Vue.component("ta-root", Root);


export const StateType = {
    Active: "Active",
    Timestamp: "Timestamp",
    InstalledPlugins: "InstalledPlugins",
    InstallPlugin: "InstalledPlugin",
    UninstallPlugin: "UninstallPlugin",
    ActivePlugin: "ActivePlugin"
}
export const state = {
    [StateType.Active]: false,
    [StateType.Timestamp]: Date.now(),
    [StateType.InstalledPlugins]: {},
    [StateType.ActivePlugin]: "",
};
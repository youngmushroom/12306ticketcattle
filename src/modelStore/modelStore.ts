import ObjectStoreSchema from "../../schema/ObjectStore.schema.json";
import DriverSchema from "../../schema/Driver.schema.json";
import { Ticket, User, Task } from "../model/model";
import AbstractStore from "../indexdb/abstractStore";
import LocalDB from "../indexdb/localDB";
let localDB = new LocalDB(DriverSchema.name, DriverSchema.version);
localDB.onDefineObjectStore((db: IDBDatabase) => {
    if (!db) {
        return;
    }
    ObjectStoreSchema.stores.forEach((objSchema) => {
        let objStore = db.createObjectStore(objSchema.name, {
            keyPath: "id",
            autoIncrement: true,
        });
        objStore.createIndex("$id", "id", {
            unique: true
        });
        if (!objSchema.fields) {
            return;
        }
        objSchema.fields.forEach((field) => {
        });
    });
});
class UserStore extends AbstractStore<User> {
    constructor() {
        super(localDB);
    }
    protected newInstance(t: User): User {
        return new User(t);
    }
    protected getObjectStoreName(): string {
        return "User";
    }
}
class TicketStore extends AbstractStore<Ticket>  {
    constructor() {
        super(localDB);
    }
    protected newInstance(t: Ticket): Ticket {
        return new Ticket(t);
    }
    protected getObjectStoreName(): string {
        return "Ticket";
    }
}
class TaskStore extends AbstractStore<Task>  {
    constructor() {
        super(localDB);
    }
    protected newInstance(t: Task): Task {
        return new Task(t);
    }
    protected getObjectStoreName(): string {
        return "Task";
    }
};
let userStore = new UserStore();
let ticketStore = new TicketStore();
let taskStore = new TaskStore();
export { userStore, ticketStore, taskStore };

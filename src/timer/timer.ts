import Defer from "../defer/defer";

class TimerStrategy {
    private _stopTime: Date;
    constructor(time: Date) {
        this.stopTime = time;
    }
    public doStrategy(): number {
        let time = this.stopTime.getTime() - new Date().getTime();
        if (time < 0) {
            // during exprie 3 minute, task is still alive
            if (time > -3 * 60 * 1000) {
                return 0;
            }
            return -1;
        } else if (time > 24 * 60 * 60 * 1000) {
            // 1 day
            return 24 * 60 * 60 * 1000;
        } else if (time > 60 * 60 * 1000) {
            // 1 hour
            return 60 * 60 * 1000;
        } else if (time > 1 * 60 * 1000) {
            // 1 minute
            return 60 * 1000;
        }
        return 0;
    }

    public set stopTime(time) {
        this._stopTime = time;
    }

    public get stopTime() {
        return this._stopTime;
    }
}
export default class Timer {
    private timerStrategy: TimerStrategy;
    constructor(stopTime: Date) {
        this.timerStrategy = new TimerStrategy(stopTime);
    }
    async run() {
        let defer = Defer.defer<void>();
        let send = async () => {
            if (!this._onRunning || "function" != typeof this._onRunning) {
                throw `no onRunning callback after send request: callback is required, it must be return a Promise<boolean>, please attach your callback to Timer#onRunning`;
            }
            let isContinue = await this._onRunning.call(this);
            if (isContinue) {
                timer();
                return;
            }
            defer.resolve();
        };
        let timer = () => {
            let timeout = this.timerStrategy.doStrategy();
            if (timeout < 0) {
                defer.reject();
                return;
            }
            setTimeout(() => {
                if (timeout < 30 * 1000) {
                    send();
                }
            }, timeout);
        }
        timer();
        return defer.promise;
    }
    private _onRunning: () => Promise<boolean>;
    onRunning(callback: () => Promise<boolean>) {
        this._onRunning = callback;
    }
}
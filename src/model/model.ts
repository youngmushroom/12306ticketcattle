export class User {
    private _name: string;
    private _idCard: string;
    private _id: string | number;
    constructor(u: User) {
        this.name = u.name;
        this.idCard = u.idCard;
        this.id = u.id;
    }
    set idCard(idCard) {
        this._idCard = idCard;
    }
    get idCard() {
        return this._idCard;
    }
    set name(name: string) {
        this._name = name;
    }
    get name() {
        return this._name;
    }
    set id(id: string | number) {
        this._id = id;
    }
    get id() {
        return this._id;
    }
}
export class Ticket {
    id: string;
    from: string;
    to: String;
    date: Date;
    trainNum: number;
    constructor(t: Ticket) {

    }
}
export class Task {
    private _id: number;
    private _userId: number;
    private _ticketId: number;
    private _startDateTime: Date;
    private _endDateTime: Date;
    private _status: TaskStatus;

    constructor(t: Task) {
        this.ticketId = t.ticketId;
        this.userId = t.userId;
        this.id = t.id;
    }

    set userId(id) {
        this._userId = id;
    }
    get userId() {
        return this._userId;
    }
    set ticketId(id) {
        this._ticketId = id;
    }
    get ticketId() {
        return this._ticketId;
    }
    set id(id) {
        this._id = id;
    }
    get id() {
        return this._id;
    }
    set startDateTime(time) {
        this._startDateTime = time;
    }
    get startDateTime() {
        return this._startDateTime;
    }
    set endDateTime(time) {
        this._endDateTime = time;
    }
    get endDateTime() {
        return this._endDateTime;
    }
    set status(status) {
        this._status = status;
    }
    get status() {
        return this._status;
    }


}
export enum TaskStatus {
    Pending = "PENDING",
    Running = "RUNNING",
    Exprie = "EXPRIE",
    Success = "SUCCESS",
    Faild = "FAILD",
}
export default { Task, Ticket, User };
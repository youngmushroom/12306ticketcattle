import { StateType } from "./state";
export const mutations = {
    [StateType.Active]: function (state, payload) {
        state[StateType.Active] = payload;
    },

    [StateType.ActivePlugin]: function (state, payload) {
        state[StateType.ActivePlugin] = payload;
    },

    [StateType.InstallPlugin]: function (state, payload) {
        state[StateType.InstalledPlugins][payload.name] = payload;
    },

    [StateType.UninstallPlugin]: function (state, payload) {
        delete state[StateType.InstalledPlugins][payload];
    },
};

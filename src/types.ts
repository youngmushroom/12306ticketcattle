
export interface IApp {
	boot(): void;
	register(plugin: IPlugin): void;
	unregister(plugin: IPlugin): void;

	activePlugin(pluginId: string): void;
	inactivePlugin(pluginId: string): void;
}

export interface IPlugin {
	id: string;
	name: string;
	store: any;
}
export interface INotification {
	send<T>(title: string, content?: string | HTMLElement): Promise<T>;
}

declare global {
	interface Window {
		ticketAngel: IApp;
	}
}
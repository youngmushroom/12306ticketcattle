export default class Defer<T> {
    private _resolve: any;
    private _reject: any;
    private _promise: Promise<T>;
    constructor() {
        this._promise = new Promise((resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        });
    }
    public resolve(value?: any) {
        this._resolve(value);
        return this._promise;
    }
    public reject(value?: any) {
        this._reject(value);
        return this._promise;
    }
    public get promise() {
        return this._promise;
    }
    static defer<T>() {
        return new Defer<T>();
    }
}
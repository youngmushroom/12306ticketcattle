import "../style/svg-icon.less"
import Vue from "vue";
import Component from "vue-class-component";
import template from "./svg.icon.temp.html";
import { Prop, Emit } from "vue-property-decorator";

@Component({
    template,
})
export class SVGIcon extends Vue {
    @Prop({ type: String })
    id: string;
    @Prop()
    title: string;
    @Prop()
    className: string;
    @Emit("click")
    onButtonClick(e: MouseEvent) {
        return e;
    }
}
Vue.component("svg-icon", SVGIcon);
import Vue from "vue";
import Component from "vue-class-component";
import { Prop, Inject, Model, Provide } from "vue-property-decorator";
import { BTable, BSpinner } from "../bootstrap";
import { message } from "../../common/util/util";
import template from "./TaskPanel.temp.html";

@Component({
    name: "task-panel",
    template,
    components: {
        "b-table": BTable,
        "b-spinner": BSpinner,
    },
})
export class TaskPanel extends Vue {
    @Prop()
    public searchText: string;
    @Model("change")
    public isBusy: boolean;
    @Model("change")
    public items: any;
    private readonly fields: Array<{ key: string, label: string, sortable?: boolean }>;
    constructor() {
        super();
        this.fields = [
            {
                key: "userName",
                label: message("extensionUserName"),
            }, {
                key: "startStationName",
                label: message("extensionStartStationName"),
            }, {
                key: "endStationName",
                label: message("extensionEndStationName"),
            }, {
                key: "startDate",
                label: message("extensionStartDate"),
                sortable: true
            }, {
                key: "taskStartDate",
                label: message("extensionTaskStartDate"),
                sortable: true
            }, {
                key: "taskStatus",
                label: message("extensionTaskStatus"),
            },
        ];
        this.items = [
            {
                taskStatus: 40,
                taskStartDate: "",
                userName: '小明',
                startStationName: '北京',
                endStationName: "上海",
                startDate: "2019/02/20",
            },
            {
                taskStatus: 40,
                taskStartDate: "",
                userName: '小红',
                startStationName: '广州',
                endStationName: "上海",
                startDate: "2019/02/21",
            },
            {
                taskStatus: 40,
                taskStartDate: "",
                userName: '小刚',
                startStationName: '西安',
                endStationName: "广州",
                startDate: "2019/02/22",
            },
        ];
    }

    onSearch(searchText: string) {

    }
}
Vue.component("task-panel", TaskPanel);

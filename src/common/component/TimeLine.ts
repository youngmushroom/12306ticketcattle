import "../style/timeline.less";
import Vue from "vue";
import Component from "vue-class-component";
import timeLineTemp from "./timeline.temp.html";
import timeLineItemTemp from "./timelineItem.temp.html";

import { Model } from "vue-property-decorator";

import "../util/util";
import { getUUID } from "../util/util";

@Component({
    template: timeLineTemp,
    name: "time-line"
})
export class TimeLine extends Vue {
    @Model("change")
    items: Array<{ id: string, date: Date }> = [];
    onEditButtonClick(item: any) {
        console.debug();
    }

    onDeleteButtonClick(id: string) {
        console.debug();
        for (let index = 0; index < this.items.length; index++) {
            const element = this.items[index];
            if (id === element.id) {
                this.items.splice(index, 1);
                console.debug("remove success");
                return;
            }
        }
    }
    onAddButtonClick() {
        this.items.push({
            id: `${Math.random()}`,
            date: new Date()
        });
        this.items.sort((d1, d2) => d1.date.getTime() - d2.date.getTime());
    }
    get uuid() {
        return getUUID();
    }
}
Vue.component("time-line", TimeLine);
@Component({
    template: timeLineItemTemp,
})
class TimeLineItem extends Vue {

}
Vue.component("time-line-item", TimeLineItem);

import jquery from "jquery";
import { ResultSet } from "../../plugins/12306/types/Passenger";
export function post<T>(url: string, data?: any) {
    console.debug(jquery);
    return jquery.ajax({
        url: encodeURI(url),
        type: "post",
        async: !1,
        dataType: "json",
        data,
    }).then((result: ResultSet<T>, status: string) => {
        if (!result || !result.data.flag) {
            Promise.reject("");
            return;
        }
        return result.data.datas;
    }, (a, b, c) => {
        return Promise.reject("");
    }, (a, b, c) => {
        return Promise.reject("");
    });
}
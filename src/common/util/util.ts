import uuid from "uuid/v1";
//const browser = { i18n: { getMessage(a, b) { return a; } } };
export const message = function (messageName: string, substitutions?: string | string[]) {
    if (!messageName /*|| !browser*/) {
        return messageName;
    }
    return messageName;//browser.i18n.getMessage(messageName, substitutions);
}

export const useSVG = function (href: string) {
    return `<svg xmlns="http://www.w3.org/2000/svg" ><use xlink:href=${href}></use></svg>`;
}

export const getUUID = function () {
    return uuid();
}
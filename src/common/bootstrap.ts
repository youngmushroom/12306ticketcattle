import Vue from "vue";
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import BAlert from 'bootstrap-vue/es/components/alert/alert';
import BTable from 'bootstrap-vue/es/components/table/table';
import BBadge from 'bootstrap-vue/es/components/badge/badge';
import BSpinner from 'bootstrap-vue/es/components/spinner/spinner';
import BModal from 'bootstrap-vue/es/components/modal/modal';
import BProgress from 'bootstrap-vue/es/components/progress/progress';
import BProgressBar from 'bootstrap-vue/es/components/progress/progress-bar';
import vBPopover from 'bootstrap-vue/es/directives/popover/popover';
import BPagination from 'bootstrap-vue/es/components/pagination/pagination';
import BPaginationNav from 'bootstrap-vue/es/components/pagination-nav/pagination-nav';
import BInputGroup from 'bootstrap-vue/es/components/input-group/input-group';
import BNavbar from 'bootstrap-vue/es/components/navbar/navbar';
import BButton from 'bootstrap-vue/es/components/button/button';
import { BFormInput as BInput } from 'bootstrap-vue/es/components/form-input/form-input';

Vue.use(BootstrapVue);
Vue.component('b-alert', BAlert);
Vue.component('b-badge', BBadge);
Vue.component('b-table', BTable);
Vue.component('b-progress', BProgress);
Vue.component('b-progress-bar', BProgressBar);
Vue.component('b-pagination', BPagination);
Vue.component('b-pagination-nav', BPaginationNav)
Vue.component('b-spinner', BSpinner);
Vue.component('b-modal', BModal)
Vue.directive('b-popover', vBPopover);
Vue.component('b-input-group', BInputGroup);
Vue.component('b-navbar', BNavbar);
Vue.component('b-button', BButton);
Vue.component('b-input', BInput);

export {
    BAlert,
    BBadge,
    BButton,
    BTable,
    BProgress,
    BProgressBar,
    BPagination,
    BPaginationNav,
    BSpinner,
    BModal,
    vBPopover,
    BInputGroup,
    BNavbar,
    BInput
};
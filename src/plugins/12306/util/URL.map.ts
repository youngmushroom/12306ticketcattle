const ROOT = "https://kyfw.12306.cn";
export const URL = {
    Passenger: {
        Add: `${ROOT}/otn/passengers/add`,
        Query: `${ROOT}/otn/passengers/query`,
        Delete: `${ROOT}/otn/passengers/delete`,
        Update: `${ROOT}/otn/passengers/query`
    },
    Order: {
        OrderNoComplete: `${ROOT}/otn/queryOrder/queryMyOrderNoComplete`,
    }
}
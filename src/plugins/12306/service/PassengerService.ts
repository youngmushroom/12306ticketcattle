import { QueryResultPassengerInfo, AddablePassengerInfo, UpsertPassengerResult, UpdatablePassengerInfo, DelectablePassengerInfo, DeletePassengerResult } from "../types/Passenger";
import { post } from "../../../common/util/Post";
import { URL } from "../util/URL.map";
export class PassengerService {
    async getAllPassengers() {
        return post<QueryResultPassengerInfo>(
            URL.Passenger.Query,
            {
                pageIndex: 1,
                pageSize: 10
            })
    }
    async addPassenger(info: AddablePassengerInfo) {
        return post<UpsertPassengerResult>(URL.Passenger.Add, info);
    }
    async updatePassenger(info: UpdatablePassengerInfo) {
        return post<UpsertPassengerResult>(URL.Passenger.Update, info);
    }
    async deletePassenger(info: DelectablePassengerInfo) {
        return post<DeletePassengerResult>(URL.Passenger.Delete, info);
    }
}

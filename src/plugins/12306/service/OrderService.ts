import { post } from "../../../common/util/Post";
import { URL } from "../util/URL.map";

export class OrderService {
    async queryOrderNoComplete() {
        return post(URL.Order.OrderNoComplete);
    }
}
import Vue from "vue";
import Component, { } from "vue-class-component";
import { message } from "../../../common/util/util";
import { Prop, Model } from "vue-property-decorator";
import { passengerService } from "../service";
import { QueryResultPassengerInfo } from "../types/Passenger";
import template from "./PassengerPanel.temp.html";
@Component({
    name: "passenger-panel",
    template,
})
export class PassengerPanel extends Vue {
    @Prop()
    public searchText: string;
    @Model("change")
    public isBusy: boolean = false;
    @Model("change")
    public items: QueryResultPassengerInfo[];
    private readonly fields: Array<{ key: string, label: string, sortable?: boolean }>;
    constructor() {
        super();
        this.fields = [
            {
                key: "passenger_name",
                label: message("extensionUserName"),
                sortable: true
            }, {
                key: "passenger_id_no",
                label: message("extensionUserIDcardNum"),
                sortable: true
            }, {
                key: "mobile_no",
                label: message("extensionMobileNum"),
                sortable: true
            }, {
                key: "activeTasks",
                label: message("extensionAction"),
            }
        ];
    }
    mounted() {
        this.loadingPassengerFrom12306();
    }
    async loadingPassengerFrom12306() {
        try {
            this.isBusy = true;
            const data = await passengerService.getAllPassengers();
            if (data) {
                this.items = data;
            }
        } finally {
            this.isBusy = false;
        }
    }
}
Vue.component("passenger-panel", PassengerPanel);
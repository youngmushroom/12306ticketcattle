import "../styles/timelinecard.less"
import Vue from "vue";
import Component, { } from "vue-class-component";
import template from "./timelinecard.temp.html";
import { Prop } from "vue-property-decorator";
@Component({
    template,
    name: "time-line-card"
})
class TimeLineCard extends Vue {
    @Prop()
    item;
}
Vue.component("time-line-card", TimeLineCard);



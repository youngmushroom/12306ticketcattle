export enum UserSelfType {
    IsMyself = "Y",
    NotMyself = "N",
}
export interface QueryResultPassengerInfo {
    address: string;
    born_date: "1980-01-01 00:00:00";
    code: "1";
    country_code: "CN";
    delete_time: "1980/06/29";
    email: string;
    first_letter: "CGF";
    gat_born_date: string;
    gat_valid_date_end: string;
    gat_valid_date_start: string;
    gat_version: string;
    isUserSelf: UserSelfType;
    mobile_no: string;
    passenger_flag: "0";
    passenger_id_no: string;
    passenger_id_type_code: PassengerIdTypeCode;
    passenger_id_type_name: "中国居民身份证";
    passenger_name: string;
    passenger_type: PassengerType,
    phone_no: string;
    postalcode: string;
    recordCount: "7";
    sex_code: SexCode;
    sex_name: "男";
    total_times: "95";
}
export interface ResultData<T> {
    datas: T[];
    flag: boolean;
}
export interface ResultSet<T> {
    data: ResultData<T>;
    httpstatus: number;
    messages: [];
    status: boolean;
    validateMessages: any;
    validateMessagesShowId: string;
}

export type UpsertPassengerResult = {
    data: { flag: boolean }
};
export type DeletePassengerResult = {
    data: { flag: boolean }
};
export enum SexCode {
    Male = "M",
    Famle = "F"
}
export type AddablePassengerInfo = {
    passenger_name: string;
    sex_code: SexCode;
    passenger_id_type_code: PassengerType;
    passenger_id_no: string;
    mobile_no?: string;
    phone_no?: string;
    email?: string;
    address?: string
    postalcode?: string;
    "studentInfoDTO.school_name"?: string;
    "studentInfoDTO.department"?: string
    "studentInfoDTO.school_class"?: string;
    "studentInfoDTO.student_no"?: string;
    "studentInfoDTO.preference_card_no"?: string;
    country_code?: "CN";
};
export interface DelectablePassengerInfo {
    passenger_name: string;
    passenger_id_type_code: PassengerType;
    passenger_id_no: string;
    isUserSelf: string;
}
export interface UpdatablePassengerInfo {
    passenger_name: string;
    passenger_id_no: string;
    mobile_no: string;
    passenger_type: PassengerType;
}
export enum PassengerType {
    Adult = 1,
    Child = 2,
    Student = 3,
    SoldiersOrOthers = 4
}
export enum PassengerIdTypeCode {
    ChinaIDCard = "1",
}
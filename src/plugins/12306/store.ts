import actions from "./actions";
import mutations from "./mutations";
import state from "./states";

export const store = {
	state,
	actions,
	mutations
};

import Vue from "vue";
import Vuex from "vuex";
import "./component/TimeLineCard";
import "./styles/app.less";
import { BButton, BModal } from "../../common/bootstrap";
import { TaskPanel } from "../../common/component/TaskPanel";

import { message } from "../../common/util/util";
import { PassengerPanel } from "./component/PassengerPanel";
import { passengerService, orderService } from "./service";
import { store } from "./store";
import Component from "vue-class-component";
import template from "./app.temp.html";

enum PanelType {
    Task = "TASK",
    Passenger = "PASSENGER"
}
@Component({
    template,
})
export class App12306 extends Vue {
    mounted() {
        this.$store.registerModule("12306", store);
    }
    destroyed() {
        this.$store.unregisterModule("12306");
    }
}
Vue.component("app-12306", App12306)
import Vue from "vue";
import "./app";
import { store } from "./store";
import { IPlugin, IApp, INotification } from "./types";
import { StateType } from "./state";
import svgIcons from "./icon.svg";

class Notification implements INotification {
    send<T>(title: string, content?: string | HTMLElement): Promise<T> {
        return Promise.resolve({} as T);
    }
}

export class App implements IApp {
    private readonly notify: INotification;
    constructor() {
        this.notify = new Notification();
    }

    boot() {
        new Vue({
            el: ".ext-ticket-angle",
            template: `<div><div style="display: none;">${svgIcons}</div><ta-root ></ta-root></div>`,
            store,
        });
    }
    unregister(plugin: IPlugin): void {
        if (!plugin) {
            return;
        }

        store.commit(StateType.UninstallPlugin, plugin);
        store.unregisterModule(plugin.name);
    }
    register(plugin: IPlugin) {
        if (!plugin) {
            return;
        }
        store.commit(StateType.InstallPlugin, plugin);
        if (plugin.store) {
            store.registerModule(plugin.name, plugin.store);
        }
    }
    activePlugin(pluginId: string) {
        const installedPlugin = store.state.InstalledPlugins[pluginId];
        if (!installedPlugin) {
            return;
        }
        store.commit(StateType.ActivePlugin, pluginId);
    }
    inactivePlugin(pluginId: string) {
        const installedPlugin = store.state.InstalledPlugins[pluginId];
        if (!installedPlugin) {
            return;
        }
        store.commit(StateType.ActivePlugin, "");
    }

    notification(title: string, content?: string | HTMLElement) {
        return this.notify.send(title, content);
    }
}

const appendEntry = () => {
    if (!document || !document.body) {
        return;
    }
    var body = document.body;
    var extEntry = document.createElement("div");
    extEntry.classList.add("ext-ticket-angle");
    if (body.firstChild) {
        body.insertBefore(extEntry, body.firstChild);
    } else {
        body.appendChild(extEntry);
    }
    return extEntry;
};

appendEntry();
const app = new App();
app.boot();
window.ticketAngel = app;
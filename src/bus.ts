import Vue from "vue";
export default new Vue();
export enum BusEventType {
    Open = "show-task",
    CreateTask = "create-task",
    Close = "close",
    Save = "save",
    Cancel = "cancel"
}
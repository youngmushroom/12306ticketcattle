export default class Driver {
    private _conn: IDBOpenDBRequest;
    constructor(name: string, version: number) {
        if (!name) {
            throw "";
        }
        this.initDB(name);
    }
    private initDB(name: string) {
        this._conn = indexedDB.open(name);
        this.addEventListener();
    }
    private addEventListener() {
        if (!this._conn) {
            return;
        }
        this._conn.addEventListener("error", (e) => {
            if (!this._onerror) {
                return;
            }
            this._onerror.call(this, e);
        });
        this._conn.addEventListener("success", (e) => {
            //
            let db = (e.target as any).result as IDBDatabase;
            if (!this._onsuccess) {
                return;
            }
            this._onsuccess.call(this, db);
        });
        this._conn.addEventListener("blocked", (e) => {
            let db = (e.target as any).result as IDBDatabase;
            if (!this._onblock) {
                return;
            }
            this._onblock.call(this, e);
        });
        this._conn.addEventListener("upgradeneeded", (e: IDBVersionChangeEvent) => {
            let db = (e.target as any).result as IDBDatabase;
            if (!this._onupgradeneeded) {
                return;
            }
            this._onupgradeneeded.call(this, db);
        });
    }
    private _onupgradeneeded: (db: IDBDatabase) => void;
    public onupgradeneeded(callback: (db: IDBDatabase) => void): Driver {
        this._onupgradeneeded = callback;
        return this;
    }
    private _onsuccess: (e: IDBDatabase) => void;
    public onsuccess(callback: (e: IDBDatabase) => void): Driver {
        this._onsuccess = callback;
        return this;

    }
    private _onerror: (e: Event) => void;
    public onerror(callback: (e: Event) => void): Driver {
        this._onerror = callback;
        return this;
    }
    private _onblock: (e: Event) => void;
    public onblock(callback: (e: Event) => void): Driver {
        this._onblock = callback;
        return this;
    }
    public getConnection() {
        return this._conn;
    }
}

import LocalDB, { ResultCode } from "./localDB";

export default abstract class AbstractStore<T> {
    protected localDB: LocalDB;
    constructor(localDB: LocalDB) {
        this.localDB = localDB;
    }
    async add(t: T): Promise<T> {
        try {
            let result = await this.localDB.upsert<T>(this.getObjectStoreName(), t);
            if (!result) {
                return;
            }
            return result.code == ResultCode.Success ? this.newInstance(result.data) : undefined
        } catch (error) {
            //
        }
    }

    async put(u: T): Promise<T> {
        try {
            let result = await this.localDB.upsert<T>(this.getObjectStoreName(), u);
            if (!result) {
                return;
            }
            return result.code == ResultCode.Success ? this.newInstance(result.data) : undefined
        } catch (error) {
            //
        }
    }

    async delete(id: number): Promise<T> {
        try {
            let result = await this.localDB.delete<T>(this.getObjectStoreName(), id);
            if (!result) {
                return;
            }
            return result.code == ResultCode.Success ? this.newInstance(result.data) : undefined;
        } catch (error) {
            //
        }
    }

    async getById(id: number): Promise<T> {
        try {
            let result = await this.localDB.get<T>(this.getObjectStoreName(), id);
            if (!result) {
                return;
            }
            return result[0] && result[0].code == ResultCode.Success ? this.newInstance(result[0].data) : undefined;
        } catch (error) {
            //
        }
    }

    async getByIds(id: number[]): Promise<T[]> {
        try {
            let result = await this.localDB.get<T>(this.getObjectStoreName(), id);
            if (!result) {
                return;
            }
            return result.map((item) => {
                return item.code == ResultCode.Success ? this.newInstance(item.data) : undefined
            }).filter(item => item);
        } catch (error) {
            //
        }
    }

    async getAll(): Promise<T[]> {
        try {
            let result = await this.localDB.get<T>(this.getObjectStoreName());
            if (!result) {
                return [];
            }
            return result.map((item) => {
                return item.code == ResultCode.Success ? this.newInstance(item.data) : undefined

            }).filter(item => item);
        } catch (error) {
            //
        }
        return [];
    }

    protected abstract getObjectStoreName(): string;
    protected abstract newInstance(t: T): T;

}
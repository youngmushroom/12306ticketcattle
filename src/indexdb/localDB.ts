import Defer from "../defer/defer";
import Driver from "./driver";

class ObjectUtils {
    static freeze(obj: any, key: string | number | symbol = "id", value?: any) {
        if (!obj || "object" !== typeof obj) {
            return obj;
        }
        let _obj;
        try {
            _obj = JSON.parse(JSON.stringify(obj));
        } catch (error) {
            //
        }
        if (_obj && _obj.hasOwnProperty(key) && undefined != _obj[key]) {
            Object.defineProperty(_obj, key, {
                configurable: false,
                enumerable: true,
                writable: false,
                value: undefined === value ? _obj[key] : value
            });
            Object.seal(_obj);
        }
        return _obj;
    }
}


export enum ResultCode {
    Success = "SUCCESS",
    Error = "ERROR",
}
interface QueryResultOption<T> {
    code: ResultCode;
    message?: string;
    error?: any;
    data?: T;
}
class QueryResult<T> {
    code: ResultCode;
    message: string;
    error: any;
    data: T;
    constructor({ code, data, message, error }: QueryResultOption<T>) {
        this.code = code;
        this.message = message;
        this.error = error;
        this.data = ObjectUtils.freeze(data, "id");
    }
}
interface UpsertResultOption<T> {
    code: ResultCode;
    data?: T;
    message?: string;
    error?: any;
    isCreated?: boolean;
}
class UpsertResult<T> {
    code: ResultCode;
    message: string;
    error: any;
    data: T;
    _isCreated: Readonly<boolean>;
    constructor({ code, data, isCreated = false, message, error }: UpsertResultOption<T>) {
        this.code = code;
        this.message = message;
        this._isCreated = isCreated;
        this.error = error;
        this.data = ObjectUtils.freeze(data, "id");
    }
    isCreated() {
        return this._isCreated;
    }
}
interface DeleteResultOption<T> {
    code: ResultCode;
    data?: T;
    message?: string;
    error?: any;
    isCreated?: boolean;
}
class DeleteResult<T> {
    code: ResultCode;
    message: string;
    error: any;
    data: T;
    _isCreated: Readonly<boolean>;
    constructor({ code, data, isCreated = false, message, error }: DeleteResultOption<T>) {
        this.code = code;
        this.message = message;
        this._isCreated = isCreated;
        this.error = error;
        this.data = ObjectUtils.freeze(data, "id");
    }
    isCreated() {
        return this._isCreated;
    }
}

export default class LocalDB {
    private _name: string;
    private _version: number;
    private _onDefineObjectStore: (db: IDBDatabase) => void;
    constructor(name: string, version: number = 1) {
        this._name = name;
        this._version = version;
    }

    public onDefineObjectStore(onDefineObjectStore: (db: IDBDatabase) => void) {
        this._onDefineObjectStore = onDefineObjectStore;
    }

    private async openObjectStoreAsync(storeName: string) {
        let defer = Defer.defer<IDBObjectStore>();
        let driver = new Driver(this.name, this.version);
        driver.onupgradeneeded(this._onDefineObjectStore);
        driver.onsuccess((db: IDBDatabase) => {
            let transaction = db.transaction([storeName], "readwrite");
            let objStore = transaction.objectStore(storeName);
            transaction.addEventListener("complete", function (e) {
                objStore;
                transaction;
                defer.resolve("complete");
            });
            transaction.addEventListener("error", function (e) {
                defer.reject(e);
            });
            transaction.addEventListener("abort", function (e) {
                defer.reject(e);
            });
            defer.resolve(objStore);
        });
        driver.onerror((e) => {
            defer.reject(e);
        });
        driver.onblock((e) => {
            defer.reject(e);
        });
        return defer.promise;
    }
    
    public async get<T>(storeName: string, query?: IDBValidKey | IDBKeyRange) {
        let defer = Defer.defer<QueryResult<T>[]>();
        let store = undefined;
        try {
            store = await this.openObjectStoreAsync(storeName);
        } catch (error) {
            defer.reject(new QueryResult({ code: ResultCode.Error, data: "", message: error }));
        }
        if (!store) {
            defer.resolve();
            return defer.promise;
        }
        let request: IDBRequest<T[]>;
        if (undefined != query) {
            request = store.get(query) as IDBRequest<T[]>;
        } else {
            request = store.getAll() as IDBRequest<T[]>;
        }
        request.addEventListener("success", (e) => {
            defer.resolve((request.result || []).map((data) => {
                return new QueryResult({ code: ResultCode.Success, data })
            }));
        });
        request.addEventListener("error", (e) => {
            defer.resolve(new QueryResult({ code: ResultCode.Error, data: undefined, error: e }));
        });
        return defer.promise;
    }

    public async upsert<T>(storeName: string, t: T): Promise<UpsertResult<T>>;
    public async upsert<T>(storeName: string, t: T[]): Promise<UpsertResult<T>[]>;
    public async upsert<T>(storeName: string, t: T | T[]) {
        let defer = Defer.defer<any>();
        let store: IDBObjectStore;
        if (!storeName || !t) {
            return defer.reject(new UpsertResult({ code: ResultCode.Error }));
        }
        try {
            store = await this.openObjectStoreAsync(storeName);
        } catch (error) {
            defer.reject(new UpsertResult({ code: ResultCode.Error }));
        }
        if (!store) {
            return defer.promise;
        }
        let updateOrInsert = async (item: T) => {
            if ("object" != typeof item) {
                return;
            }
            if (item.hasOwnProperty("id") && undefined != (item as T & { id: string | number | symbol })["id"]) {
                return this.put(store, item as T);
            } else {
                return this.add(store, item);
            }
        };
        let updateOrInsertLoopCore = async (item: Array<T>, result: Array<UpsertResult<T>>, defer: Defer<Array<UpsertResult<T>>>) => {
            if (!item || item.length < 1) {
                defer.resolve(result);
                return;
            }
            let upsertedItem = await updateOrInsert(item.shift());
            result.push(upsertedItem);
            updateOrInsertLoopCore(item, result, defer);
        };
        let updateOrInsertLoop = () => {
            let defer = Defer.defer<Array<UpsertResult<T>>>();
            updateOrInsertLoopCore(t as T[], [], defer);
            return defer.promise;
        }

        if (Array.isArray(t)) {
            defer.resolve(await updateOrInsertLoop());
        } else {
            defer.resolve(await updateOrInsert(t as T & { id: string | number | symbol }));
        }
        return defer.promise;
    }
    private async add<T>(store: IDBObjectStore, t: T) {
        let defer = Defer.defer<UpsertResult<T>>();
        if (!t) {
            defer.resolve();
            return;
        }
        let request = store.add(t);
        request.addEventListener("success", (e) => {
            (t as any)["id"] = request.result;
            defer.resolve(new UpsertResult({ code: ResultCode.Success, data: t, isCreated: true }));
        });
        request.addEventListener("error", (e) => {
            defer.resolve(new UpsertResult({ code: ResultCode.Error, data: t }));
        });
        return defer.promise;
    }
    private async put<T>(store: IDBObjectStore, t: T) {
        let defer = Defer.defer<UpsertResult<T>>();
        let request = store.put(t);
        request.addEventListener("success", (e) => {
            defer.resolve(new UpsertResult({
                code: ResultCode.Success,
                data: t
            }));
        });
        request.addEventListener("error", (e) => {
            defer.resolve(new UpsertResult({
                code: ResultCode.Error,
                data: t,
                error: e
            }));
        });
        return defer.promise;
    }
    public async delete<T>(storeName: string, query: string | number | symbol): Promise<DeleteResult<T>>;
    public async delete<T>(storeName: string, query?: IDBValidKey | IDBKeyRange): Promise<DeleteResult<T>>;
    public async delete<T>(storeName: string, query?: any): Promise<any> {
        return null;
    }

    public get name() {
        return this._name;
    }

    public get version() {
        return this._version;
    }
}
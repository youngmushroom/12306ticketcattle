import DriverSchema from "../schema/Driver.schema.json";
import Driver from "../src/indexdb/driver.js";
jasmine.DEFAULT_TIMEOUT_INTERVAL = 3000;
describe("driver", function () {
  beforeEach(() => {
    indexedDB.deleteDatabase(DriverSchema.name);
  });
  it("driver call order", function () {
    let spy = jasmine.createSpyObj("spy", ["onupgradeneeded", "onsuccess", "onerror", "onblock"]);
    let driver = new Driver(DriverSchema.name, DriverSchema.version);
    driver.onupgradeneeded(spy.onupgradeneeded);
    driver.onblock(spy.onblock);
    driver.onerror(spy.onerror);
    driver.onsuccess(async () => {
      await spy.onsuccess();
      expect(driver).not.toBeUndefined();
      expect(driver.getConnection()).not.toBeUndefined();
      expect(spy.onupgradeneeded).toHaveBeenCalled();
      expect(spy.onupgradeneeded).toHaveBeenCalledBefore(spy.ontransaction)
      expect(spy.onsuccess).toHaveBeenCalled();
      expect(spy.onsuccess).toHaveBeenCalledBefore(spy.ontransaction)
      expect(spy.ontransaction).toHaveBeenCalled();
    });
  });
});